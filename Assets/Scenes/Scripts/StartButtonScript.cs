﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartButtonScript : MonoBehaviour {

	public GameObject[] FirstHiddenObjects;

	public GameObject[] FirstShownObjects;

	public GameObject howToImage;

	public GameObject[] leftRightInputBtns;


    [SerializeField]
    private GameObject RightTap;

    [SerializeField]
    private GameObject LeftTap;

	//public GameObject hideBgPanel;

	//[SerializeField]
	//private Image imgHowTo;

	public static bool gamePlayed = false;

	//public static float bannerTransition = 500;

	//public GameObject banner;

	private bool howToShown = false;

	private Soundmanager Snd_mngr
	{
		get{
			if(_snd_mngr == null){
				_snd_mngr = GameObject.Find("Audiomanager")
					.GetComponent<Soundmanager>();
			}
			return _snd_mngr;
		}
	}
	Soundmanager _snd_mngr;

	// Use this for initialization
	void Start () {
		Application.targetFrameRate = 60; // ターゲットフレームレートを60に設定

		if(gamePlayed){
			startBtnClicked();
		}

		//DontDestroyOnLoad (banner);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			if(gamePlayed){
			}else{
				if(howToShown){
					ShowHideHowTo();
				}else{
					Application.Quit();
				}

			}
			
		}
	}

	public void startBtnClicked(){
		foreach(GameObject go in FirstHiddenObjects){
			go.SetActive(true);
		}

		Snd_mngr.PlayMusic ();

		if (GameObject.Find ("BannerButton") != null) {
			GameObject.Find ("BannerButton").GetComponent<BannerCtrl> ().HideBanner ();
		}

		foreach(GameObject go in FirstShownObjects){

			//if(go.tag == "banner"){
			//	go.GetComponent<RectTransform>().anchoredPosition += 
			//		bannerTransition * Vector2.up;
			//}else
			{
				go.SetActive(false);
			}

		}

		/*GameObject banner = GameObject.FindWithTag ("banner");
		if(banner != null){
			banner.GetComponent<RectTransform>().anchoredPosition = 
				bannerTransition * Vector2.up;
		}*/
		//BannerCtrl.component.HideBanner ();

		if(PlayerPrefs.GetInt("Score") == 0){
			howToImage.SetActive(true);

			/*foreach(GameObject go in leftRightInputBtns){
				go.SetActive(false);
			}*/
			//hideBgPanel.SetActive(true);
		}

		//gameObject.SetActive (false);

	}

	public void ShowHideHowTo()
	{
		if (!gamePlayed) {
			howToShown = !howToShown;
			
			foreach (Transform children in transform) {
				children.gameObject
					.SetActive (!children.gameObject.activeSelf);
			}
		} else {
			howToImage.SetActive(false);

			/*foreach(GameObject go in leftRightInputBtns){
				go.SetActive(true);
			}*/
			//hideBgPanel.SetActive(false);
		}

	}

}
