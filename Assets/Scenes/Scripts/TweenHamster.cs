﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TweenHamster : MonoBehaviour {

	[SerializeField]
	float maxWaitTime = 5f;

	[SerializeField]
	float tweenTime = 0.1f;

	// Use this for initialization
	void Start () {
		StartCoroutine (TurnCoroutine());
	}

	IEnumerator TurnCoroutine(){

		while(true){
			yield return new WaitForSeconds(
				tweenTime + maxWaitTime * Random.value);

			int sign = (Random.value < 0.5f) ? -1 : 1;

			Vector3 rot = transform.rotation.eulerAngles;

			transform.DOLocalRotate(
				rot + sign * new Vector3(0, 180, 0), tweenTime);

			transform.position = new Vector3(
				0, transform.position.y, transform.position.z);

		}

	}

	// Update is called once per frame
	void Update () {
	
	}
}
